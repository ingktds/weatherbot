#!/usr/bin/env python
"""
"""

import requests
import configparser
import json
from datetime import datetime
from pytz import timezone
from pprint import pprint


def load_config(configfile='Weatherbotfile'):
    """
    load config
    """
    config = configparser.ConfigParser()
    config.read(configfile)
    return config


def get_weather_data(q="Tokyo,jp", lang="ja", units="metric"):
    """
    get data from openweatherapi
    """
    base_url = "http://api.openweathermap.org/data/2.5/forecast"
    config = load_config()
    api_key = config["openweatherapi"]["apikey"]
    
    r = requests.get("{}?q={}&appid={}&lang={}&units={}".format(base_url,
        q, api_key, lang, units))
    return r.json()


def get_token():
    """
    function to get token
    """
    config = load_config()
    base_url = config["rocketchat"]["url"]
    endpoint = "/api/v1/login"
    username = config["rocketchat"]["username"]
    password = config["rocketchat"]["password"]
    payload = {"username": username, "password": password}
    r = requests.post("{}{}".format(base_url, endpoint), data=payload)
    data = r.json()
    return data["data"]


def post_message(msg):
    """
    function to post message
    """
    config = load_config()
    base_url = config["rocketchat"]["url"]
    endpoint = "/api/v1/chat.postMessage"
    channel = config["rocketchat"]["channel"]
    payload = {"channel": channel, "text": msg, "alias": "天気予報"}
    data = get_token()
    headers = {"X-Auth-Token": data["authToken"], "X-User-Id": data["userId"], "Content-type": "application/json"}
    r = requests.post("{}{}".format(base_url, endpoint),
        data=json.dumps(payload), headers=headers)
    return r


def create_message(data):
    """
    fuction to create message
    """
    fmt = "{:<17}\t{:<8}\t{}\n"
    msg = fmt.format("日時", "天気", "気温(℃)")
    msg += "=" * 47 + "\n"
    for d in data["list"][0:8]:
        dt = datetime.fromtimestamp(d["dt"])
        dt_tz = dt.astimezone(timezone("Asia/Tokyo"))
        date = dt_tz.strftime("%Y-%m-%d %H:%M")
        weather = d["weather"][0]["description"]
        temp = d["main"]["temp"]
        msg += fmt.format(date, weather, temp)
    quote = "```"
    return quote + msg + quote


def main():
    """
    main function
    """
    data = get_weather_data()
    msg = create_message(data)
    post_message(msg)


if __name__ == "__main__":
    main()
